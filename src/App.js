import './App.css';
import {
  ApolloClient, 
  InMemoryCache, 
  ApolloProvider, 
} from "@apollo/client";
import {onError} from "@apollo/client/link/error"
import GetBookings from './Components/GetBookings';

const errorLink = onError(({ graphqlErrors, networkError }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({message, location, path}) => {
      alert(`Graphql error ${message}`);
    }); 
  }
});


const client = new ApolloClient({
  errorLink,
  uri: "https://qz1l95.sse.codesandbox.io/",
  cache: new InMemoryCache()
})


function App() {
  return <ApolloProvider client={client}>
    {" "}
    <GetBookings />
  </ApolloProvider>
}



export default App;
