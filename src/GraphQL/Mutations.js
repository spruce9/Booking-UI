import { gql } from "@apollo/client";

export const CREATE_BOOKING_MUTATION = gql`

    mutation createBooking(
        $name: string!
        $email: string!
        $address: string!
        $type: string!
        $date: string!
        ) {
        createBooking(
            name: $name
            email: $email
            address: $address
            type: $type
            date: $date 
            )
        
        }

`