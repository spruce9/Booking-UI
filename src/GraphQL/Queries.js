import {gql} from "@apollo/client"

export const LOAD_BOOKINGS = gql `
    query ($filterBy: BookingFilterInput) {
        bookings(filterBy: $filterBy) {
          total
          bookings {
            id
            email
            type
            status
            name
            address
            serviceDate
          }
        }
      }
`