import { render, screen } from '@testing-library/react';
import App from './App';

test('renders get Bookings page', () => {
  render(<App />);
  const linkElement = screen.getByText(/Create Booking/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders create booking button', () => {
  render(<App />);
  const linkElement = screen.getByTestId("createBooking");
  expect(linkElement).toBeTruthy();
});

