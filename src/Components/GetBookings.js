import React, { useEffect, useState } from 'react'
import {useQuery, gql, from } from "@apollo/client"
import {LOAD_BOOKINGS } from "../GraphQL/Queries"
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import logo from '../logo.svg'
import { useModal } from 'react-hooks-use-modal';
import {CREATE_BOOKING_MUTATION} from '../GraphQL/Mutations'
import { useMutation } from '@apollo/client'
import { 
  Navbar, 
  Form, 
  Container, 
  Row, 
  Col, 
  Button, 
  CloseButton
} from 'react-bootstrap'


function GetBookings() {
  const {error, loading, data} = useQuery(LOAD_BOOKINGS)
  const [bookings, setBookings] = useState([]);
  
  useEffect(() => {  
   setTimeout(() =>{
    setBookings(data.bookings.bookings);
   },200)
  }, [data]);

  const columns=[{
    dataField:"name",
    text:"Customer",
    sort:true,
  },
  {
    dataField:"email",
    text:"Email",
    sort:true
  },
  {
    dataField:"address",
    text:"Address",
    sort:true
  },
  {
    dataField:"type",
    text:"Booking Type",
    sort:true
  },
  {
    dataField:"serviceDate",
    text:"Booking Date + Time",
    sort:true
  },
  {
    dataField: "view",
    text: " ",
    sort: false,
    formatter: rankFormatter,
    headerAttrs: { width: 50 },
    attrs: { width: 50, class: "EditRow" } 
  }]

  const options = {
    pageStartIndex: 1,
    sizePerPage: 20,
    hideSizePerPage: false,
    hidePageListOnlyOnePage: false
  };

  const [Modal, open, close, isOpen] = useModal('root', {
    preventScroll: false,
    closeOnOverlayClick: false
  });

  const [validated, setValidated] = useState(false);

  const [createBooking ] = useMutation(CREATE_BOOKING_MUTATION);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
    createBooking({
      variables: {
        bookingName: bookingName,
        bookingEmail: bookingEmail,
        bookingAddress: bookingAddress,
        bookingCity: bookingCity,
        bookingState: bookingState,
        bookingZipcode: bookingZipcode,
        bookingType: bookingType,
        bookingDate: bookingDate,
        bookingTime: bookingTime
      }
    })

    if (error) {
      console.log(error);
    }
  };

  const [bookingName, setBookingName] = useState("");
  const [bookingEmail, setBookingEmail] = useState("");
  const [bookingAddress, setBookingAddress] = useState("");
  const [bookingCity, setBookingCity] = useState("");
  const [bookingState, setBookingState] = useState("");
  const [bookingZipcode, setBookingZipcode] = useState("");
  const [bookingDate, setBookingDate] = useState("");
  const [bookingTime, setBookingTime] = useState("");
  const [bookingType, setBookingType] = useState("");


  return (

    <div className='App'>
        <div className='Body'>   
           <div className='NavBar'>
             <Navbar bg="white" variant="dark">
                 <img src={logo} alt="Spruce"/>
             </Navbar>
           </div>

          <h1>Bookings</h1>
          <button data-testid= "createBooking" type="button" id='createBookingBtn' className="CreateBooking" onClick={open}>Create Booking</button>
          <Modal id="bookingModal">
            <div>

              <h1>Create Booking</h1>
              <CloseButton onClick={close}/>
              <Form  data-testid= "modalForm" noValidate validated={validated} onSubmit={handleSubmit}>
                 <Container >
                   <Row>
                     <Col><Form.Control 
                            type="text" 
                            placeholder="Name" 
                            id="bookingName" 
                            onChange={(e)=>{setBookingName(e.target.value);}} 
                            required/>
                     </Col> 
                     <Col><Form.Control 
                            type="email" 
                            placeholder="email" 
                            id="bookingEmail" 
                            onChange={(e)=>{setBookingEmail(e.target.value);}} 
                            required/>
                     </Col>   
                   </Row>
                   <Row>
                     <Col><Form.Control 
                            type="text" 
                            placeholder="Street Address" 
                            id="bookingAddress" 
                            onChange={(e)=>{setBookingAddress(e.target.value);}} 
                            required/></Col>
                   </Row>
                   <Row>
                     <Col><Form.Control 
                            type="text" 
                            placeholder="City" 
                            id="bookingCity" 
                            onChange={(e)=>{setBookingCity(e.target.value);}} 
                            required/>
                     </Col>
                     <Col><Form.Control 
                            type="text" 
                            placeholder="State" 
                            id="bookingState" 
                            onChange={(e)=>{setBookingState(e.target.value);}} 
                            required/>
                     </Col>
                     <Col><Form.Control 
                            type="text" 
                            placeholder="Zipcode" 
                            id="bookingZipcode" 
                            onChange={(e)=>{setBookingZipcode(e.target.value);}} 
                            required/>
                     </Col>
                   </Row>              
                   <Row>
                     <Col>
                       <Form.Select onChange={(e)=>{setBookingType(e.target.value);}} aria-label="Default select example" id="bookingType" >
                         <option> </option>
                         <option value="Housekeeping">House keeping</option>
                         <option value="DogWalk">Dog Walk</option>
                       </Form.Select>
                     </Col>
                   </Row>
                   <Row>
                     <Col><Form.Control 
                            type={"date"} 
                            placeholder={"Booking Date"} 
                            id={"bookingDate"} 
                            onChange={(e)=>{setBookingDate(e.target.value);}} 
                            required/>
                     </Col>   
                     <Col><Form.Control 
                            type={"time"} 
                            placeholder={"Booking Time"} 
                            id={"bookingTime"} 
                            onChange={(e)=>{setBookingTime(e.target.value);}} 
                            required/>
                     </Col>   
                   </Row>
                 </Container>
                 <div>
                   <Button type="submit" class='btn-primary'>Create Booking</Button>{' '}
                 </div>
              </Form> 
             
            </div>
          </Modal>
          <div className='table'> 
             <BootstrapTable 
               keyField={"id"} 
               data={bookings}
               columns={columns} 
               hover
               condensed
               pagination={paginationFactory(options)}
              //  cellEdit={cellEditFactory({ mode: "click" })}
             /> 
          </div>
        </div> 
    </div>

  );
}

function rankFormatter(cell, row, rowIndex, formatExtraData) { 
  return ( 
    < div 
      style={{ textAlign: "center",
      cursor: "pointer",
      lineHeight: "normal" }}
    >
      <Button href="../view/bookings">View</Button>
    </div> 
); } 


export default GetBookings